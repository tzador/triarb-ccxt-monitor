const ccxt = require("ccxt");
const fs = require("fs");
const chalk = require("chalk");

if (!fs.existsSync("./logs")) {
  fs.mkdirSync("./logs");
}

function at_least(str, length) {
  while (str.length < length) str = str + " ";
  return str;
}
const fee = 0.00075
const fee3 = (1 - fee)*(1 - fee)*(1 - fee);
const best_of_exchange = {};
function show_best() {
  const sorted = [];
  for (const exchange_id in best_of_exchange) {
    sorted.push([exchange_id, best_of_exchange[exchange_id]]);
  }
  sorted.sort((a, b) => b[1][0] - a[1][0]);
  if (sorted.length > 0) {
    for (const [exchange_id, triangle] of sorted) {
      const profit =
        triangle[0] > 1 / fee3
          ? chalk.green(triangle[0].toFixed(6))
          : chalk.red(triangle[0].toFixed(6));
      console.log(at_least(exchange_id, 16), profit, triangle[1]);
    }
    console.log();
  }
  setTimeout(show_best, 1000);
}
setTimeout(show_best, 1000);

async function monitor(exchange_id) {
  async function fetch_and_log() {
    const exchange = new ccxt[exchange_id]();
    const tickers = await exchange.fetchTickers();

    let base_coins = new Set();
    let quote_coins = new Set();
    for (const symbol in tickers) {
      const [base_coin, quote_coin] = symbol.split("/");
      base_coins.add(base_coin);
      quote_coins.add(quote_coin);
    }

    let triangles = [];
    for (const quote_coin1 of quote_coins) {
      for (const quote_coin2 of quote_coins) {
        const ticker1 = tickers[quote_coin2 + "/" + quote_coin1];
        if (!ticker1) continue;
        if (ticker1.disabled) continue;
        for (const base_coin of base_coins) {
          {
            const ticker2 = tickers[base_coin + "/" + quote_coin1];
            const ticker3 = tickers[base_coin + "/" + quote_coin2];
            if (!ticker2 || !ticker3) continue;
            if (ticker2.disabled || ticker3.disabled) continue;
            if (exchange_id === "liquid")
              if (ticker2.ask > 0) {
                const profit = (1.0 / ticker2.ask) * ticker3.bid * ticker1.bid;
                triangles.push([
                  profit,
                  `↻ ${at_least(quote_coin1, 5)} buy=> ${at_least(
                    base_coin,
                    5
                  )} sell=> ${at_least(quote_coin2, 5)} sell=> ${at_least(
                    quote_coin1,
                    5
                  )}`
                ]);
              }

            if (ticker1.ask > 0 && ticker3.ask > 0) {
              const profit = (1.0 / ticker1.ask / ticker3.ask) * ticker2.bid;
              triangles.push([
                profit,
                `↺ ${at_least(quote_coin1, 5)} buy=> ${at_least(
                  quote_coin2,
                  5
                )}  buy=> ${at_least(base_coin, 5)} sell=> ${at_least(
                  quote_coin1,
                  5
                )}`
              ]);
            }
          }
        }
      }
    }
    triangles = triangles.filter(a => a[0] > 1);
    triangles.sort((a, b) => b[0] - a[0]);
    if (triangles.length > 0) {
      const filename = `./logs/${exchange_id}.txt`;
      fs.appendFileSync(filename, new Date().toJSON() + "\n");
      for (const triangle of triangles) {
        fs.appendFileSync(
          filename,
          triangle[0].toFixed(6) + " " + triangle[1] + "\n"
        );
      }
      fs.appendFileSync(filename, "\n");
      best_of_exchange[exchange_id] = triangles[0];
    }
  }

  async function loop(timeout) {
    try {
      await fetch_and_log();
      setTimeout(() => loop(1000), 1000);
    } catch (e) {
      setTimeout(() => loop(Math.round(timeout * 1.2)), timeout);
    }
  }
  setTimeout(() => loop(1000), 5000);
}

let args = process.argv;
args = args.slice(2);
if (args.length > 0) {
  args.forEach(monitor);
} else {
  ccxt.exchanges.forEach(monitor);
}
